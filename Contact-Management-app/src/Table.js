import React from 'react'
import TableRow from './TableRow'

const Table = (props) => (
  <div className="div-list">
    <h1 className="heading">Contacts List</h1>
    <table>
      <tbody>
        <tr>
          <th>Name</th>
          <th>Phone</th>
          <th>Delete</th>
          <th>Update</th>
        </tr>
        {props.data.map((contact, index) => (
          <TableRow key={contact.name+" "+contact.phone} row={contact}
            handleDelete={props.handleDelete}
            handleUpdate={props.handleUpdate}
            contactIndex={index} />
        ))}
      </tbody>
    </table >
    <br />
    <button className="add-button" onClick={props.handleAdd}>Add</button>
  </div >
)
export default Table
