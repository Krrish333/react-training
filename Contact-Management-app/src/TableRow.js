import React from 'react'

const TableRow = (props) => (
  <tr>
    <td>{props.row.name}</td>
    <td>{props.row.phone}</td>
    <td><button onClick={() => props.handleDelete(props.row.id)} >Delete</button></td>
    <td><button onClick={() => props.handleUpdate(props.contactIndex)}>Update</button></td>
  </tr>
)

export default TableRow