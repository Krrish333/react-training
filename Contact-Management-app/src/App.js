import React from 'react'
import './App.css'
import Parent from './Parent.js'

const App = () => (
  
  <div className="App">
    <Parent />
  </div>
)
export default App
