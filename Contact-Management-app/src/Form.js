import React from "react"

export default class Form extends React.Component {
  state = {
    name: "",
    phone: ""
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.index !== null && prevState.name === "") {
      return {
        name: nextProps.contact.name,
        phone: nextProps.contact.phone
      }
    }
    return null
  }

  change = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  onSave = () => {
    if (this.state.name.length > 0 && this.state.phone.length > 0)
      this.props.toUpdateContactList(this.state)
  }

  render() {
    return (
      <div className="div-login">
        <h1 className="heading">Form</h1>
        <form>
          <input
            className="text-field"
            type="text"
            name="name"
            placeholder=" Name"
            value={this.state.name}
            onChange={e => this.change(e)} /><br />
          <input
            className="text-field"
            type="text"
            name="phone"
            placeholder="Phone"
            value={this.state.phone}
            onChange={e => this.change(e)} /><br />
          <button type="submit" onClick={(e) => this.onSave(e)}>Save</button>
        </form>
      </div>
    );
  }
}
