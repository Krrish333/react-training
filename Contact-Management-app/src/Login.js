import React from "react"
import "./style.css"

class Login extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      email: "",
      password: "",
    }
  }

  change = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  render() {
    return (
      <div className="div-login">
        <h1 className="heading">Login Page</h1>
        <form>
          <input
            className="text-field"
            type="email"
            name="email"
            placeholder="Login Id"
            value={this.state.email}
            onChange={e => this.change(e)}
            required />
          <br />

          <input
            className="text-field"
            type="password"
            name="password"
            placeholder="Password"
            value={this.state.password}
            onChange={e => this.change(e)}
            required />
          <br />
          <button type="submit" onClick={() => this.props.onClick(this.state)}>Submit</button>
        </form>
      </div>
    );
  }
}
export default Login;