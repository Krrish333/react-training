import React from 'react'
import Login from './Login.js'
import Table from './Table.js'
import Form from './Form.js'

class Parent extends React.Component {
  constructor() {
    super()
    this.state = {
      currentComponent: null,
      id: "",
      contacts: [
        { id: 1, name: "Leanne Graham", phone: "12345" },
        { id: 2, name: "Ervin Howell", phone: "12345" },
        { id: 3, name: "Clementine Bauch", phone: "12345" },
        { id: 4, name: "Patricia Lebsack", phone: "12345" },
      ]
    }
  }

  loginSubmit = (data) => {
    if (data.email.length > 0 && data.password.length > 0) {
      this.setState({
        currentComponent: "table"
      });
    }
    else alert("invalid credentials")
  }

  handleAdd = () => {
    this.setState({
      currentComponent: "form",
      id: null,
    });
  };

  handleDelete = (index) => {
    const contacts = this.state.contacts
    contacts.splice(index, 1);
    this.setState({
      contacts,
      currentComponent: "table"
    });
  }

  handleUpdate = (index) => {
    this.setState({
      currentComponent: "form",
      id: index
    });
  }

  updateAndAddContact = (contactObj) => {
    if (this.state.id !== null) {
      const contacts = this.state.contacts
      contacts[this.state.id] = { id: this.state.id, name: contactObj.name, phone: contactObj.phone }
      this.setState({
        currentComponent: "table",
        contacts
      })
    }
    else {
      const newContact = {
        id: this.state.contacts.length + 1,
        name: contactObj.name,
        phone: contactObj.phone,
      }
      this.setState({
        currentComponent: "table",
        contacts: [...this.state.contacts, newContact]
      });
    }
  }

  render() {

    if (this.state.currentComponent === "table") {
      return <Table data={this.state.contacts} handleAdd={this.handleAdd}
        handleDelete={this.handleDelete}
        handleUpdate={this.handleUpdate} />
    }

    if (this.state.currentComponent === "form") {
      return <Form contact={this.state.contacts[this.state.id]}
        toUpdateContactList={contactObj => this.updateAndAddContact(contactObj)}
        index={this.state.id} />
    }

    return (
      <div className="App">
        <Login onClick={this.loginSubmit} />
      </div>
    );
  }
}
export default Parent;